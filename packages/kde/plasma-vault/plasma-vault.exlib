# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Plasma applet and services for creating encrypted vaults"

LICENCES="GPL-2 LGPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    offline-only-vaults [[ description = [
        Allows to specify vaults which can only be opened when offline
    ] ]]
"

KF5_MIN_VER="5.17.0"
QT_MIN_VER="5.12.0"

DEPENDENCIES="
    build+run:
        kde/libksysguard:4
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        offline-only-vaults? (
            kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}]
        )
    recommendation:
        sys-fs/cryfs [[ description = [ One possible crypto backend to use ] ]]
    suggestion:
        sys-fs/encfs [[ description = [
            Backend which has some security issues (https://defuse.ca/audits/encfs.htm)
        ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'offline-only-vaults KF5NetworkManagerQt'
)

