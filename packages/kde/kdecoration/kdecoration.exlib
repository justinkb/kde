# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Plugin based library to create window decorations"

DESCRIPTION="KDecoration2 is a library to create window decorations. These
window decorations can be used by for example an X11 based window manager
which re-parents a Client window to a window decoration frame.

The library consists of two parts:
* Decoration API for implementing a Decoration theme
* Private API to implement the backend part (e.g. from Window Manager side)
"

LICENCES="LGPL-2.1"
SLOT="4"
MYOPTIONS=""

if ever at_least 5.17.90 ; then
    KF5_MIN_VER="5.65.0"
elif ever at_least 5.16.90 ; then
    KF5_MIN_VER="5.62.0"
else
    KF5_MIN_VER="5.58.0"
fi
QT_MIN_VER="5.12.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

# 3 of 3 tests need a running X server
RESTRICT="test"

