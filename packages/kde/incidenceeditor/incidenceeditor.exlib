# Copyright 2016-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Library providing an incidence editor"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.60.0
QT_MIN_VER=5.11.0

DEPENDENCIES="
    build+run:
        kde/calendarsupport[>=${PV}]
        kde/eventviews[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/kdiagram[>=1.4.0]
        kde/kdepim-apps-libs[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        !kde/kdepim[<16.03.80] [[
            description = [ incidenceeditor has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
"

# 3 of 3 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DKDEPIM_ENTERPRISE_BUILD:BOOL=FALSE )

