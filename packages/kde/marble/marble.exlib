# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon kdeedu.exlib, which is:
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008-2011 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE EDU: Virtual Globe and World Atlas"
HOMEPAGE="https://marble.kde.org/ ${HOMEPAGE}"

LICENCES="
    FDL-1.2 LGPL-2.1
    BSD-3 [[ note = [ CMake scripts ] ]]
    CCPL-Attribution-3.0 [[ note = [ City files taken from geonames.org ] ]]
    public-domain [[ note = [ boundaryplacemarks.kml from the CIA World Factbook ] ]]
"
MYOPTIONS="examples
    aprs-device [[ description = [ Read from an APRS device connected via a serial port ] ]]
    qt-only     [[ description = [ Build a version only based on Qt, without KF5 ] ]]
    qtwebengine [[ description = [ Use QtWebEngine to display web content; build some web related plugins ] ]]
    tools       [[ description = [ Build various tools, e.g. for file format conversion ] ]]
"

KF5_MIN_VER=5.7.0
QT_MIN_VER=5.7.0

DEPENDENCIES="
    build:
        dev-lang/perl:*
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        media-libs/phonon[qt5(+)]
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = [ might be optional, but currently fails to build ] ]]
        x11-misc/shared-mime-info[>=0.40]
        aprs-device? ( x11-libs/qtserialport:5[>=${QT_MIN_VER}] )
        examples? ( media-libs/opencv )
        !qt-only? (
            kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
            kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
            kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
            kde-frameworks/kio:5[>=${KF5_MIN_VER}]
            kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
            kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
            kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
            kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        )
        qtwebengine? ( x11-libs/qtwebengine:5[>=${QT_MIN_VER}] )
        tools? ( dev-libs/protobuf:= )
    run:
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
"

# apparently 53 of 53 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # unwritten
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Location:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Positioning:BOOL=TRUE
    # Only supported with Qt4
    -DEXPERIMENTAL_PYTHON_BINDINGS:BOOL=FALSE
    -DICON_INSTALL_DIR=/usr/share/icons
    -DMARBLE_DATA_PATH=/usr/share/marble
    -DMOBILE:BOOL=FALSE
    -DQT_PLUGINS_DIR=/usr/$(exhost --target)/lib/qt5/plugins
    -DWITH_KF5:BOOL=TRUE
    # Marble, requires sci-geosciences/gpsd - http://gpsd.berlios.de/ and liblocation
    -DWITH_libgps=OFF
    -DWITH_liblocation=OFF
    # plugin to display ESRI Shapefiles via libshp http://shapelib.maptools.org/
    -DWITH_libshp=OFF
    # unwritten, retrieves the current geographic position from available WLAN networks
    # http://sourceforge.net/projects/libwlocate/
    -DWITH_libwlocate=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'examples MARBLE_EXAMPLES'
    'tools MARBLE_TOOLS'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'aprs-device Qt5SerialPort'
    'qtwebengine Qt5WebEngine'
    'qtwebengine Qt5WebEngineWidgets'
    'tools Protobuf'
    'tools ZLIB'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS+=( '!qt-only KF5' )
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_MARBLE_TESTS:BOOL=TRUE -DBUILD_MARBLE_TESTS:BOOL=FALSE'
)

marble_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

marble_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

