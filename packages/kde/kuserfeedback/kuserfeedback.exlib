# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde

export_exlib_phases src_prepare

SUMMARY="Framework for collecting user feedback for applications via telemetry and surveys"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        doc? ( x11-libs/qttools:5 )
    build+run:
        x11-libs/qtbase:5[>=5.8]
        x11-libs/qtcharts:5
        x11-libs/qtdeclarative:5
        x11-libs/qtsvg:5
"

kuserfeedback_src_prepare() {
    kde_src_prepare

    # Disable tests which need a running X server
    edo sed \
        -e "/uf_add_test(datasourcetest/d" \
        -e "/uf_add_test(openglinfosourcetest.cpp/d" \
        -e "/uf_add_test(selectionratiosourcetest.cpp/d" \
        -e "/uf_add_test(feedbackconfigtest/d" \
        -i autotests/CMakeLists.txt
}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_CLI:BOOL=TRUE
    -DENABLE_CONSOLE:BOOL=TRUE
    -DENABLE_SURVEY_TARGET_EXPRESSIONS:BOOL=TRUE
    # Disable the server component for now
    -DENABLE_PHP:BOOL=FALSE
    -DENABLE_PHP_UNIT:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    'doc DOCS'
)

