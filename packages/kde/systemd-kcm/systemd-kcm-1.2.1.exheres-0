# Copyright 2014-2015 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ translations=ki18n ]

SUMMARY="systemd control module for KDE"
DESCRIPTION="
Provides a graphical frontend for the systemd daemon, which allows for viewing
and controlling systemd units and logind sessions, as well as modifying
configuration files. Integrates in the System Settings dialog in KDE.
"
DOWNLOADS="mirror://kde/stable/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( linguas:
        ast ca cs da de es fi fr gl ja nl pl pt pt_BR ru sk sv uk zh_CN zh_TW
    )
"

KF5_MIN_VER=5.1.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        sys-apps/systemd[>=209]
        x11-libs/qtbase:5[>=5.2.0]
        !sys-apps/kcmsystemd [[
            description = [ systemd-kcm replaces kcmsystemd ]
            resolution = uninstall-blocked-before
        ]]
"

