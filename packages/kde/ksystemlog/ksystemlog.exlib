# Copyright 2013, 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="A system log viewer tool"
DESCRIPTION=" This program is developed for beginner users, who don't know how
to find information about their Linux system, and don't know where log files
are. It is also of course designed for advanced users, who quickly want to
understand problems of their machine with a more powerful and graphical tool
than tail -f and less commands."
HOMEPAGE+=" http://www.kde.org/applications/system/${PN}/"

LICENCES="GPL-2"
MYOPTIONS="
    journald [[ description = [ View the systemd journal with ksystemlog ] ]]
"

KF5_MIN_VER=5.30.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        journald? ( virtual/pkg-config )
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.2.0][gui]
        journald? ( sys-apps/systemd )
"

# 4 of 4 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( Journald )

