# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=annulen project=webkit release=${PNV} suffix=tar.xz ]
require cmake

SUMMARY="Qt Cross-platform application framework: QtWebkit"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    LGPL-2
"
SLOT="5"
MYOPTIONS="
    gstreamer    [[ description = [ HTML5 video/audio support via gstreamer ] ]]
    qtmultimedia [[ description = [ HTML5 video/audio support via QtMultimedia ] ]]

    ( gstreamer qtmultimedia ) [[ number-selected = at-most-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.10.0]
        dev-lang/python:*[>=2.7.0]
        dev-lang/ruby:*[>=1.9]
        dev-util/gperf[>=3.0.1]
        sys-devel/bison[>=2.1]
        sys-devel/flex[>=2.5.34]
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        dev-libs/icu:=
        dev-libs/libxml2:2.0[>=2.8.0]
        dev-libs/libxslt[>=1.1.7]
        media-libs/fontconfig
        media-libs/libpng:=
        media-libs/libwebp:=
        media-libs/woff2
        office-libs/hyphen
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/qtbase:${SLOT}[>=5.2.0][gui]
        x11-libs/qtdeclarative:${SLOT}[>=5.2.0]
        x11-libs/qtwebchannel:${SLOT}[>=5.2.0]
        gstreamer? (
            dev-libs/glib:2[>=2.36]
            media-libs/gstreamer:1.0[>=1.0.3]
            media-plugins/gst-plugins-bad:1.0[>=1.0.3] [[ note = [ mpegts, tag, gl ] ]]
            media-plugins/gst-plugins-base:1.0[>=1.0.3]
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qtmultimedia? ( x11-libs/qtmultimedia:5[>=5.2.0] )
"

# Tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPORT=Qt
    -DCMAKE_BUILD_TYPE=Release
    # QtSensor - unwritten
    -DENABLE_DEVICE_ORIENTATION:BOOL=FALSE
    -DENABLE_GAMEPAD_DEPRECATED:BOOL=FALSE
    -DENABLE_GEOLOCATION:BOOL=FALSE
    -DENABLE_NETSCAPE_PLUGIN_API:BOOL=FALSE
    -DENABLE_OPENGL:BOOL=TRUE
    -DENABLE_PRINT_SUPPORT:BOOL=TRUE
    -DENABLE_QT_WEBCHANNEL:BOOL=TRUE
    -DENABLE_WEBKIT2:BOOL=TRUE
    -DENABLE_X11_TARGET:BOOL=TRUE
    -DENABLE_XSLT:BOOL=TRUE
    -DGENERATE_DOCUMENTATION:BOOL=FALSE
    -DUSE_LD_GOLD:BOOL=FALSE
    -DUSE_LIBHYPHEN:BOOL=TRUE
    -DUSE_LIBJPEG:BOOL=TRUE
    # "At this moment, OpenMP is only used as an alternative implementation
    # to native threads for the parallelization of the SVG filters."
    -DUSE_OPENMP:BOOL=FALSE
    -DUSE_WOFF2:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_USES+=(
    GSTREAMER
    'qtmultimedia QT_MULTIMEDIA'
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DENABLE_API_TESTS:BOOL=TRUE -DENABLE_API_TESTS:BOOL=FALSE'
)

