# Copyright 2011 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require telepathy-kde [ telepathy_qt_ver_min="0.9.5" ]

SUMMARY="The telepathy-kde handler for Text Chats"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    contact-aggregation [[ description = [ Support aggregation of contacts from multiple sources ] ]]
    tts                 [[ description = [ Support for text to speech ] ]]
"

QT_MIN_VER="5.7.0"

DEPENDENCIES+="
    build+run:
        kde-frameworks/karchive:5
        kde-frameworks/kcmutils:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kdbusaddons:5
        kde-frameworks/kemoticons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/kitemviews:5
        kde-frameworks/knotifications:5
        kde-frameworks/knotifyconfig:5
        kde-frameworks/kservice:5
        kde-frameworks/ktextwidgets:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/kxmlgui:5
        kde-frameworks/sonnet:5
        net-im/ktp-common-internals[otr]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        contact-aggregation? ( kde-frameworks/kpeople:5 )
        tts? ( x11-libs/qtspeech:5 )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'contact-aggregation KF5People'
    'tts Qt5TextToSpeech'
)

BUGS_TO="mixi@exherbo.org"

