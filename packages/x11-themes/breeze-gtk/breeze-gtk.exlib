# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde
require utf8-locale python [ blacklist=2 multibuild=false ]

export_exlib_phases pkg_setup src_prepare src_configure

SUMMARY="GTK+ 2 and GTK+ 3 themes built to match KDE's breeze"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/sassc
        dev-python/pycairo[python_abis:*(-)?]
        kde/breeze[>=5.14.90]
        virtual/pkg-config
    run:
        x11-libs/gdk-pixbuf:2.0 [[ note = [ for gtk+:2 ] ]]
        x11-libs/gtk+:2
        x11-libs/gtk+:3[>=3.20] [[ note = [ see below ] ]]
    suggestion:
        kde/kde-gtk-config [[ description = [ KDE configuration module to configure appearance of GTK applications ] ]]
"

breeze-gtk_pkg_setup() {
    require_utf8_locale
}

breeze-gtk_src_prepare() {
    kde_src_prepare

    # TODO: Find a nicer solution for this; the non-deprecated FindPython
    # module currently doesn't allow to select a python version.
    edo sed -e "s/if(CMAKE_VERSION VERSION_LESS 3.12.0)/if(TRUE)/" \
            -i cmake/FindPythonCairo.cmake
}

breeze-gtk_src_configure() {
    local myconf=(
        $(kf5_shared_cmake_params)
        -DPYTHON_EXECUTABLE="/usr/host/bin/python$(python_get_abi)"
    )

    ecmake "${myconf[@]}"
}

