# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ]
require test-dbus-daemon gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="A framework for searching and managing metadata"

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-db/lmdb
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        !kde/baloo:4[-only-libraries(+)] [[
            description = [ kde-frameworks/baloo:5 and kde/baloo:4 install the same
                            binaries, so you can only install them for one package ]
            resolution = manual
        ]]
        !kde/baloo:4[<4.14.3-r5] [[
            description = [ Collision of an icon file ]
            resolution = uninstall-blocked-after
        ]]
        !kde/kde-runtime[<4.12.80] [[
            description = [ kde/kde-runtime<4.12.80 and baloo collide ]
            resolution = uninstall-blocked-after
        ]]
    run:
        kde-frameworks/kded:5
"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_EXPERIMENTAL:BOOL=FALSE )

baloo_src_prepare() {
    kde_src_prepare

    # These tests need access to the system dbus
    edo sed -e '/fileindexerconfigtest/d' \
            -e '/filtereddiriteratortest/d' \
            -e '/set(fileWatch_SRC filewatchtest.cpp/,+5d' \
            -i autotests/unit/file/CMakeLists.txt
}

