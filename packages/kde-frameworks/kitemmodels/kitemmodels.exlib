# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde

SUMMARY="Set of item models extending the Qt model-view framework"
DESCRIPTION="
KItemModels provides the following models:
* KBreadcrumbSelectionModel - Selects the parents of selected items to create
  breadcrumbs
* KCheckableProxyModel - Adds a checkable capability to a source model
* KDescendantsProxyModel - Proxy Model for restructuring a Tree into a list
* KLinkItemSelectionModel - Share a selection in multiple views which do not
  have the same source model
* KModelIndexProxyMapper - Mapping of indexes and selections through proxy
  models
* KRecursiveFilterProxyModel - Recursive filtering of models
* KSelectionProxyModel - A Proxy Model which presents a subset of its source
  model to observers"

LICENCES="LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    test:
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
"

# 7 of 7 tests need a running X server (5.12.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
)

